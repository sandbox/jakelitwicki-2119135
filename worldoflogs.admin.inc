<?php

/**
 *  Copyright (c) 2012 Litwicki Media
 *  Written for Sacrifice Guild - www.sacrificeguild.org
 *  @author: Thezdin, thezdin@sacrificeguild.org
 *  @license: http://www.gnu.org/copyleft/gpl.html
 *
 */

/**
 *	Implement hook_form
 */
function worldoflogs_config_guilds_form($form, &$form_state) {

  $form['worldoflogs_gids'] = array(
  	'#type' => 'textfield',
  	'#title' => t('Guilds'),
  	'#description' => t('Comma separated list of Guild IDs from worldoflogs.com'),
  	'#multiple' => TRUE,
  	'#default_value' => variable_get('worldoflogs_gids', ''),
  	'#attributes' => array(
  		'placeholder' => t('12345, 67890'),
  	),
  );

  // $form['worldoflogs_front_page_minimum_rank'] = array(
  // 	'#type' => 'textfield',
  // 	'#title' => t('Minimum Promoted Rank'),
  // 	'#description' => t('The minimum rank # to publish to front page.'),
  // 	'#default_value' => variable_get('worldoflogs_front_page_minimum_rank', ''),
  // );

  // $form['worldoflogs_interval'] = array(
  // 	'#type' => 'textfield',
  // 	'#title' => t('Interval'),
  // 	'#description' => t('How often (hours) should we sync with worldoflogs.com?'),
  // 	'#default_value' => variable_get('worldoflogs_interval', ''),
  // 	'#attributes' => array(
  // 		'placeholder' => t('Enter the interval of hours between data sync.'),
  // 	),
  // );

  return system_settings_form($form);

}